# Experimental PRIMO Visualisation

This is a curated combination of different student projects aimed at implementing a visualization for Primo and graphical models in general. jpoeppel took features from different implementations and fixed some major bugs for this most stable version. Not all features there were implemented at some point are currently fully supported.

This implementation uses react.js frontend to render the graph in a webbrowser. Currently flask is used as a simple webserver that handles the communication between Python and Javascript.

## Installation

The easiest way to install the gui is by using the conda package available from the scs channel. Instructions for a local installation are below.

## Requirements:

### Python
The minimal Python requirements are listed in requirements.txt and can be easily installed using 

pip install -r requirements.txt

### node js

The frontend in javascript has its own requirements which are defined in primo_gui_react/package.json .
These requirements can be installed using npm.

## Local installation

### Python
The Python backend can simply be installed used:

`python setup.py [install,develop]`

### Frontend

When checking out the project locally, you will need to build the frontend files. Use

`npm run build`

in the primo_gui_react folder for this. This will create a "build" folder with the html and js files which 
the flask server will then serve.


## Start:

The visualisation can be run by using the script 'runPrimo_gui.py'. The basic usage is to specify a xbif or conf as an optional first argument. The gui will load the specified model and start the webserver. If no model was specified, it will start with an empty Bayesian Network.

## Additinal parameters:

* --port: Specifies the port to run at (Default: 8080)
* --debug: Starts the webserver in debug mode (Default: Disabled)
* --browser/--no-browser: Enables/disables starting a webbrowser directly. (Default: Enabled)


## Controls:

### Drawing a Graph

* Use the "Add Node" and "Add Edge" buttons in the sidebar to select the respective modes
* Nodes are added by clicking in the main graph view when "Add Node" is selected
* Edges are added by clicking and holding from the starting node and releasing on the target node when "Add Edge" is selected
* You can change a node's position when "Default" is selected, by clicking and dragging a node.

### Setting CPTs

* When adding new edges, a red exlamation mark will appear on the child node, indicating that its CPTs are not valid
* RIGHT-CLICK on the node when "Default" is selected to bring up the context menu
* Check "Show Info" to open the node's CPT, from which you an also change its name and the node's outcomes
    * You can change any (changeable) value by DOUBLE-CLICKING on that value, e.g. the node's name.
    * You need to confirm your changes for them to take effect.
    
        
### Delete Nodes / Edges

* Nodes or edges can be deleted by RIGHT-CLICKING the node/edge when "Default" is selected from the context menu.

### Moving and Zooming the Graph

* Moving:
    * DRAG and DROP the Chart (DRAG a 'blank' zone not an element) when "Default" is selected      
* Zooming in and out:
    * Use the MOUSE-WHEEL for zooming and focusing
    
## Navigation Bar
### File

* Create a new Graph/load an existing graph (xbif format) or save your current graph.


### View

* Set viewing options (the nodes, their outcomes in the circles, their "legends" and info panels) for all nodes at once
    
### Inference Method

* Select an 'Inference Method':
    * Go over 'Inference Method' to select a method
    * A repeated CLICK on the (same) Method renews the calculation
        * 'Inference Methods' in Discrete Baysian Network:
            1. 'Variable Elimination'
            2. 'Factor Tree'
            3. 'Gibbs Sampling'
            4. 'Metropolis Hasting'
* Info: No Method is choosen as default. Instead, an uniform distribution is used 
which also may arise if an error is detected. 
* Selecting sampling based methods (Gibbs Sampling and Metropolis Hastings) will open additional sampling options, where you can configure the number of samples to use and whether or not to replace all samples


### Visualisation

* The GUI can visualize Gibbs Sampling by showing the state of each drawn sample and if desired, the currently sampled node's Markov Blanket
* You can additionally (and in parallel to any inference method) visualize DSeparation.
    * When changing the evidence in the graph you will need to "Run" dSeparation again before those changes 
        take effect.
 
## Running:

* When installed (e.g. using pip), the script runPrimo_gui.py will be added to the binary path, meaning that the gui can then be started by simply calling 'runPrimo_gui.py [Path_to_xbif]'


## Features "available" but not currently integrated in this version

- Visualizing Dynamic Bayesian Networks
- Showing Markov Equivalent Graphs for Bayesian Networks
- Performing value queries (most probable explanation)

These could be integrated with varying degrees of effort if desired. 
Integrating DBNs may be the hardest as that implementation still used d3.js instead of react so fundamental changes
would be required to integrate it in this current version.