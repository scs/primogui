import React, { useState, useEffect } from 'react';
import './App.css';
import Graph from './components/Graph';

function App() {

  const [fileName, setFileName] = useState('new-graph.xbif');
  const [graphData, setGraphData] = useState(null);
  const [id, setId] = useState(0);

  //loads initial Graph
  useEffect(() => {
    fetch('/_getGraph', {method: 'POST', headers: {'Content-Type': 'application/json'}, body: JSON.stringify({height: window.innerHeight, width: window.innerWidth, Radius: 50})})
    .then(res => res.json()).then(data => {
      setGraphData(data[0]);
    });
  }, []);

  const newGraph = (newFileName, data) => {
    setId(id+1);
    setFileName(newFileName);
    setGraphData(data);
  }
  

  return (
    <div className="App">
      {(graphData !== null) ? (
        <Graph key={id} initialGraphData={graphData} fileName={fileName} newGraph={newGraph}/>
      ) : null}
    </div>
  );

}

export default App;
