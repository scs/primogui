import React, { useState, Fragment, useRef, useEffect } from 'react';
import {
  Drawer,
  Divider,
  List,
  ListItem,
  ListItemText,
  ListSubheader,
  Menu,
  MenuItem,
  Button,
  ButtonGroup,
  CircularProgress,
  Slider, ListItemSecondaryAction, Switch, IconButton, Grid, Checkbox
} from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import CallMadeIcon from '@material-ui/icons/CallMade';
import ControlCameraIcon from '@material-ui/icons/ControlCamera';
import './Sidebar.css';
import InputSlider from './InputSlider';

const ListItemStyle = {
  paddingTop: 0,
  paddingBottom: 0,
}

const ButtonStyle = {
  paddingLeft: 2,
  paddingRight: 2,
}

// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
// Taken from https://stackoverflow.com/questions/62522994/how-do-i-lower-the-rate-of-http-requests-triggered-due-to-onchange-for-material
function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};

// Taken from https://overreacted.io/making-setinterval-declarative-with-react-hooks/
function useInterval(callback, delay) {
  const savedCallback = useRef();

  // Remember the latest callback.
  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  // Set up the interval.
  useEffect(() => {
    function tick() {
      savedCallback.current();
    }
    if (delay !== null) {
      let id = setInterval(tick, delay);
      return () => clearInterval(id);
    }
  }, [delay]);
}


const Sidebar = ({mouseMode, updateGraph, nodes, inferenceTypes, currentInferenceMethod, 
            setMouseMode, updateAllStates, newGraph, fileName, sampleStep, setSampleStep, showCurrentSample, 
            toggleShowCurrentSample, showCurrentMarkovBlanket, toggleShowCurrentMarkovBlanket,
            performDSeparation, dSeparation, updateDSeparation, visualizeDSeparation, updateVisualizeDSeparation}) => {
  const [showViewMenu, setShowViewMenu] = useState(false);
  const [showFileMenu, setShowFileMenu] = useState(false);
  const [fullChange, setFullChange] = useState(false);
  const [samplesValid, setSamplesValid] = useState(false);
  const [numSamples, setNumSamples] = useState(1000);
  const [sampleOnChange, setSampleOnChange] = useState(true);
  const [loadingSamples, setLoadingSamples] = useState(false);
  const [playSamples, setPlaySamples] = useState(false);
  const [startNode, setStartNode] = useState("Select");
  const [endNode, setEndNode] = useState("Select");

  const increaseSamples = () => {
    if (sampleStep < numSamples-1) {
      setSampleStep(sampleStep => sampleStep+1)
    } else {
      setPlaySamples(false);
    }
  }

  useInterval(increaseSamples, playSamples ? 500 : null);

  const toggleViewMenu = (e) => {
    if (showViewMenu) {
      setShowViewMenu(false);
    } else {
      setShowViewMenu(e.currentTarget)
    }
  }

  const toggleFileMenu = (e) => {
    if (showFileMenu) {
      setShowFileMenu(false);
    } else {
      setShowFileMenu(e.currentTarget)
    }
  }

  const changeFile = (e) => {
    const file = e.target.files[0];
    const reader = new FileReader();
    reader.readAsText(file, 'UTF-8')
    reader.onload = () => {
      fetch('/_loadGraph', {method: 'POST', headers: {'Content-Type': 'application/json'}, 
      body: JSON.stringify({'content': reader.result, 'path': file.name, 'width': window.innerWidth, 'height': window.innerHeight})})
      .then(res => res.json()).then(data => {
        newGraph(file.name, data[0]);
      });
    }
  }

  const loadNewGraph = (e) => {
    fetch('/_newGraph', {method: 'POST', headers: {'Content-Type': 'application/json'}, body: JSON.stringify({height: window.innerHeight, width: window.innerWidth, Radius: 50})})
    .then(res => res.json()).then(data => {
      newGraph('new-graph.xbif', data[0]);
    });  
  }
  
  const changeMethod = (inferenceMethod) => {
    let _fullChange = fullChange;
    if (inferenceMethod == "Visualise Samples") {
      setFullChange(false);
      _fullChange = false
    }
    if (playSamples) {
      setPlaySamples(false);
    }

    // if (inferenceMethod !== "dSeparation") {
    //   updateDSeparation({paths: null});
    // }

    setSamplesValid(false);
    fetch('/_changeMethod', {method: 'POST', headers: {'Content-Type': 'application/json'}, body: JSON.stringify({method: inferenceMethod, fullChange: _fullChange})})
    .then(res => res.json()).then(data => {
      setLoadingSamples(false);
      setSamplesValid(true);
      updateGraph(data[0]);
    });
  }

  const changeNumSamples = (numSamples) => {
    setNumSamples(numSamples);
    if (numSamples < sampleStep) {
      setSampleStep(numSamples-1);
    }
    if (playSamples) {
      setPlaySamples(false);
    }
    setSamplesValid(false);
    if (sampleOnChange) {
      setLoadingSamples(true);
      fetch('/_changeNumSamples', {method: 'POST', headers: {'Content-Type': 'application/json'}, body: JSON.stringify({numSamples: numSamples})})
      .then(res => res.json()).then(data => {
        setLoadingSamples(false);
        setSamplesValid(true);
        updateGraph(data[0]);
      });
    }
  }

  const changeFullChange = (event) => {
    let newFullChange = event.target.checked;
    setSamplesValid(false);
    fetch('/_changeFullChange', {method: 'POST', headers: {'Content-Type': 'application/json'}, body: JSON.stringify({fullChange: newFullChange})})
    .then(res => res.json()).then(data => {
      setFullChange(newFullChange);
      setLoadingSamples(false);
      setSamplesValid(true);
      updateGraph(data[0]);
    });
  }

  const changeSampleOnChange = (event) => {
    let newSampleOnChange = event.target.checked;
    setSampleOnChange(newSampleOnChange)
  }

  const changePath = (pathIdx) => {
    updateDSeparation({pathIndex: pathIdx-1})
  }

  const triggerSampling = (event) => {
    setLoadingSamples(true);
    fetch('/_drawSamples', {method: 'POST', headers: {'Content-Type': 'application/json'}, body: JSON.stringify({numSamples: numSamples})})
    .then(res => res.json()).then(data => {
      setLoadingSamples(false);
      setSamplesValid(true);
      updateGraph(data[0]);
    });
  }

  const saveFile = () => {
    fetch('/_getGraphString', {method: 'GET', headers: {'Content-Type': 'application/json'}})
    .then(res => res.json()).then(data => {
      var FileSaver = require('file-saver');
      var blob = new Blob([data[0]]);
      FileSaver.saveAs(blob, fileName);
    });
  }

  const toggleSamplePlay = () => {
    var newPlaySamples = !playSamples
    setPlaySamples(newPlaySamples);
  }

  var options = []; 

  function handleSelectChange(event) {
    if (event.target.name === "startNode") {
      // setStartNode(event.target.value);
      updateDSeparation({startNode: event.target.value})
    }
    if (event.target.name === "endNode") {
      // setEndNode(event.target.value);
      updateDSeparation({endNode: event.target.value})
    }
}
  if (dSeparation.paths) {
  console.log("sidebar paths: ", dSeparation.paths);
  }
  if (currentInferenceMethod === "Visualise Samples" && sampleStep !== null) {
    options.push(
      <div>
      <ListItem style={ListItemStyle}>
        <ListItemText primary="Gibbs Sampling:" sx={{fontWeight: "bold"}} />
      </ListItem>
      <ListItem style={{padding: 0}}>
        <Slider variant="determinate" value={sampleStep+1} min={0} max={numSamples-1} step={1} onChange={(e, newValue) => setSampleStep(newValue)}/>
      </ListItem>
      <ListItem style={{padding: 0}}>
      <ButtonGroup variant='text'>
        <Button onClick={() => setSampleStep(0)}>Start</Button>
        <Button onClick={() => setSampleStep(sampleStep - 1)} disabled={sampleStep < 1 ? true : false}>-1</Button>
        <Button onClick={() => setSampleStep(sampleStep + 1)} disabled={sampleStep > numSamples-2 ? true : false}>+1</Button>
        <Button onClick={() => setSampleStep(numSamples-1)}>End</Button>
        <Button onClick={() => toggleSamplePlay()}>{playSamples ? "Pause" : "Play"}</Button>
      </ButtonGroup>
      </ListItem>
      <ListItem style={ListItemStyle}>
        <ListItemText primary={'Current Sample'}/>
        <ListItemSecondaryAction>
          <Checkbox 
            checked={showCurrentSample}
            onChange={toggleShowCurrentSample}
            disableRipple
            size='small'
            style={{backgroundColor: 'transparent', padding: '0px'}}
          />
        </ListItemSecondaryAction>
      </ListItem>
      <ListItem style={ListItemStyle}>
        <ListItemText primary={'Markov Blanket'}/>
        <ListItemSecondaryAction>
          <Checkbox 
            checked={showCurrentMarkovBlanket}
            onChange={toggleShowCurrentMarkovBlanket}
            disableRipple
            size='small'
            style={{backgroundColor: 'transparent', padding: '0px'}}
          />
        </ListItemSecondaryAction>
      </ListItem>
      </div>
      )
  }

  if (visualizeDSeparation) {
    options.push(
    <div>
    <ListItem style={ListItemStyle}>
      <ListItemText primary="DSeparation:" style={{fontWeight: "bold"}}/>
    </ListItem>
    <ListItem style={ListItemStyle}>
      <ListItemText primary="Start:"/>
      <select name="startNode"  value={dSeparation.startNode} onChange={handleSelectChange}>
        <option value="Select">Select</option>
        {nodes.map((node, index) =>
          <option value={node.name}>{node.name}</option>
      )
      }
      </select>
    </ListItem>
    <ListItem style={ListItemStyle}>
      <ListItemText primary="End:"/>
      <select name="endNode" value={dSeparation.endNode} onChange={handleSelectChange}>
      <option value="Select">Select</option>
        {nodes.map((node, index) =>
          <option value={node.name}>{node.name}</option>
      )
      }
      </select>
    </ListItem>
    <Button onClick={performDSeparation}>
      RUN
    </Button>
    {(dSeparation.paths != null && dSeparation.paths.length > 0) ? (
      <div>
        <InputSlider id="dSepPath-slider" 
            default={dSeparation.pathIndex+1} 
            label="Paths" 
            minValue={1} 
            maxValue={dSeparation.paths.length} 
            step={1}
            onChange={debounce(changePath, 100)}
            width={100}
        />
       </div>
      ) : <ListItemText primary="No open path found."/>}
    {/* {(dSep.paths != null) ? (
      <div>
        <Button onClick={() => setActiveDSeparation({...dSep, pathIndex: dSep.pathIndex-1})} disabled={activeDSeparation().pathIndex < 1 ? true : false}>-1</Button>
          {(activeDSeparation().paths[0].length > 0) ? (
            <ListItem>
              <ListItemText primary={activeDSeparation().pathIndex+1}/>
              <ListItemText primary="/"/>
              <ListItemText primary={activeDSeparation().paths[0].length}/>
            </ListItem>
          ) : (
            <ListItem style={ListItemSelectedStyle}>
            <ListItemText primary="No paths found!"/>
            </ListItem>
          )}

        <Button onClick={() => setActiveDSeparation({...activeDSeparation(), pathIndex: activeDSeparation().pathIndex+1})} disabled={activeDSeparation().pathIndex >= activeDSeparation().paths[0].length-1 ? true : false}>+1</Button>
      </div>
    ) : null} */}
  </div>)
  }

  return ( 
    <Drawer variant="permanent" anchor="left" width={180}>
      <List>
        <ListSubheader>
          General
        </ListSubheader>
        <ListItem style={ListItemStyle} id='anchor' button onClick={toggleFileMenu}>
          <ListItemText primary='File' />
          <Menu
            open={Boolean(showFileMenu)}
            anchorEl={showFileMenu}
            getContentAnchorEl={null}
            anchorOrigin={{ vertical: "center", horizontal: "right" }}
            transformOrigin={{ vertical: "center", horizontal: "left" }}
            keepMounted
            onClose={() => setShowFileMenu(false)}
          >
            <ListItem style={ListItemStyle} button onClick={loadNewGraph} component='label'>
              <ListItemText primary='New Graph' />
            </ListItem>
            <ListItem style={ListItemStyle} button component='label'>
              <ListItemText primary='Load Graph' />
              <input
                type="file"
                accept='.xbif'
                style={{ display:'none'}}
                onChange={changeFile}
              />
            </ListItem>
            <ListItem style={ListItemStyle} button onClick={saveFile} component='label'>
              <ListItemText primary='Save Graph' />
            </ListItem>
          </Menu>
        </ListItem>
        <ListItem style={ListItemStyle} id='anchor' button onClick={toggleViewMenu}>
          <ListItemText primary='View' />
          <Menu
            open={Boolean(showViewMenu)}
            anchorEl={showViewMenu}
            getContentAnchorEl={null}
            anchorOrigin={{ vertical: "center", horizontal: "right" }}
            transformOrigin={{ vertical: "center", horizontal: "left" }}
            keepMounted
            onClose={() => setShowViewMenu(false)}
          >
            <MenuItem>
              <ListItemText>Nodes</ListItemText>
              <Button color="primary" onClick={() => updateAllStates('hide', false)}>Show All</Button>
              <Button color="secondary" onClick={() => updateAllStates('hide', true)}>Hide All</Button>
            </MenuItem>
            <MenuItem>
              <ListItemText>Outcomes</ListItemText>
              <Button color="primary" onClick={() => updateAllStates('showOutcomes', true)}>Show All</Button>
              <Button color="secondary" onClick={() => updateAllStates('showOutcomes', false)}>Hide All</Button>
            </MenuItem>
            <MenuItem>
              <ListItemText>Legend</ListItemText>
              <Button color="primary" onClick={() => updateAllStates('showLegend', true)}>Show All</Button>
              <Button color="secondary" onClick={() => updateAllStates('showLegend', false)}>Hide All</Button>
            </MenuItem>
            <MenuItem>
              <ListItemText>Info</ListItemText>
              <Button color="primary" onClick={() => updateAllStates('showInfo', true)}>Show All</Button>
              <Button color="secondary" onClick={() => updateAllStates('showInfo', false)}>Hide All</Button>
            </MenuItem>
          </Menu>
        </ListItem>
        <Divider/>
        <ListSubheader>
          Edit
        </ListSubheader>
        <ListItem style={{padding: '0px'}}>
          <ButtonGroup size='small' disableElevation>
            <Button onClick={() => setMouseMode('Drag')} style={(mouseMode === 'Drag') ? {backgroundColor: '#d6d6d6', ...ButtonStyle} : ButtonStyle}>
              <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
              >
                <ControlCameraIcon/>
                <span style={{fontSize: '10px'}}>Default</span>
              </Grid>
            </Button>
            <Button disableRipple onClick={() => setMouseMode('Node')} style={(mouseMode === 'Node') ? {backgroundColor: '#d6d6d6', ...ButtonStyle} : ButtonStyle}>
              <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
              >
              <FiberManualRecordIcon />
                <span style={{fontSize: '10px'}}>Add Node</span>
              </Grid>
            </Button>
            <Button onClick={() => setMouseMode('Link')} style={(mouseMode === 'Link') ? {backgroundColor: '#d6d6d6', ...ButtonStyle} : ButtonStyle}>
              <Grid
                  container
                  direction="column"
                  justify="center"
                  alignItems="center"
              >
              <CallMadeIcon/>
                <span style={{fontSize: '10px'}}>Add Edge</span>
              </Grid>
            </Button>
          </ButtonGroup>
        </ListItem>
      <Divider/>
      <ListSubheader>
        Inference
      </ListSubheader>
        <ListItem style={ListItemStyle} button onClick={() => changeMethod('')} selected={(currentInferenceMethod === '') ? true : false}>
          <ListItemText primary='None'/>
        </ListItem>
      {inferenceTypes.map((type, index) =>
        type === 'Visualise Samples' ? null : (
            <ListItem style={ListItemStyle} key={index} button onClick={() => changeMethod(type)} selected={(currentInferenceMethod === type) ? true : false}>
              <ListItemText primary={type}/>
            </ListItem>
      )
      )}
      {currentInferenceMethod in {"Visualise Samples":true, "Gibbs Sampling":true, "Metropolis Hastings":true} ? (
      <Fragment>
      <ListSubheader>
          Sampling Options
      </ListSubheader>
      <ListItem style={ListItemStyle}>
            <ListItemText primary={'Sample On Change? '}/>
            <ListItemSecondaryAction>
              <Checkbox 
                checked={sampleOnChange}
                onChange={changeSampleOnChange}
                disableRipple
                size='small'
                style={{backgroundColor: 'transparent', padding: '0px'}}
              />
            </ListItemSecondaryAction>
      </ListItem>
      <ListItem style={ListItemStyle}> 
        <Button 
              onClick={triggerSampling}
              style={{border: 'solid', ...ButtonStyle}}
              disabled={sampleOnChange}
        >
          Draw Samples
        </Button>
        {loadingSamples ? <CircularProgress size={30}/>: <ListItemText primary={samplesValid ? ' Samples Ready' : ' Invalid Samples'}/> }
      </ListItem>
      <ListItem style={ListItemStyle} >
        <InputSlider id="sample-slider" 
            default={1000} 
            label="Number of Samples" 
            minValue={1} 
            maxValue={50000} 
            step={1}
            onChange={debounce(changeNumSamples, 300)}
        />
      </ListItem>
      <ListItem style={ListItemStyle}>
            <ListItemText primary={'Full Change'}/>
            <ListItemSecondaryAction>
              <Checkbox 
                disabled={sampleStep !== null}
                checked={fullChange}
                onChange={changeFullChange}
                disableRipple
                size='small'
                style={{backgroundColor: 'transparent', padding: '0px'}}
              />
            </ListItemSecondaryAction>
      </ListItem>
      </Fragment>) : null}
      <Divider/>
      <ListSubheader>
        Visualisation
      </ListSubheader>
      <ListItem style={ListItemStyle} button onClick={() => changeMethod('Visualise Samples')} selected={(currentInferenceMethod === 'Visualise Samples') ? true : false}>
        <ListItemText primary={'Gibbs Sampling'}/>
      </ListItem>
      <ListItem style={ListItemStyle} button onClick={updateVisualizeDSeparation} selected={visualizeDSeparation}>
          <ListItemText primary='DSeparation'/>
        </ListItem>
        {/* <ListItem style={ListItemStyle} button onClick={() => changeMethod('MPE')} selected={(currentInferenceMethod === 'MPE') ? true : false}>
          <ListItemText primary='MPE'/>
        </ListItem> */}
        {/* <ListItem style={ListItemStyle} button onClick={() => changeMethod('Markov EQ')} selected={(currentInferenceMethod === 'dSeparation') ? true : false}>
          <ListItemText primary='Markov EQ'/>
        </ListItem> */}
        {options.length > 0? 
        (<Fragment>
        <Divider/>
        <ListSubheader>
          Options
        </ListSubheader>
        {options}
        </Fragment>): null}
      </List>
    </Drawer>
  );
}
 
export default Sidebar;