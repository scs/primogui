import React, { useState, Fragment } from 'react';
import Node from './Node';
import NodeMenu from './NodeMenu';
import OutcomeLegend from './OutcomeLegend';
import RightClickMenu from './RightClickMenu';
import InvalidError from './InvalidError';

const NodeContainer = ({id, node, radius, activateNodeDragging, mouseMode, viewPosition, zoom, getParents, updateGraph, changeNodeName, setNewLinkStart, setNewLinkEnd, nodeState, updateNodeState, getMarkovBlanket, sampleStep, showCurrentSample}) => {
  const [menu, setMenu] = useState(null);

  const openMenu = (e) => {
    setMenu({x: e.clientX, y: e.clientY})
  }

  const closeMenu = () => {
    setMenu(null);
  }

  const toggleShowOutcomes = () => {
    updateNodeState(node.name, {...nodeState, showOutcomes: !nodeState.showOutcomes})
    setMenu(null);
  }

  const toggleShowLegend = () => {
    updateNodeState(node.name, {...nodeState, showLegend: !nodeState.showLegend})
    setMenu(null);
  }
  
  const toggleHide = () => {
    if (!nodeState.hide) {
    updateNodeState(node.name, {...nodeState, hide: true})
    } else {
      updateNodeState(node.name, {...nodeState, hide: false})
    }
    setMenu(null);
  }

  const toggleShowInfo = () => {
    updateNodeState(node.name, {...nodeState, showInfo: !nodeState.showInfo})
    setMenu(null);
  }

  const showMarkovBlanket = () => {
    getMarkovBlanket(node.name);
    setMenu(null);
  }

  const getVisualisationData = () => {
    if (!(Array.isArray(node.outcomes[0].value)) || sampleStep === null || sampleStep > node.outcomes[0].value.length) {
      return node;
    } else if (!showCurrentSample) {
      return {...node, outcomes: [...node.outcomes].map(outcome => ({...outcome, value: outcome.value[sampleStep][2]}))}
    } else {
      return {...node, outcomes: [...node.outcomes].map(outcome => ({...outcome, value: outcome.value[sampleStep][0]}))}
    }
  }

  return (
    <Fragment>
      <Node
        id={id}
        node={getVisualisationData()}
        radius={radius}
        activateNodeDragging={activateNodeDragging}
        mouseMode={mouseMode}
        openMenu={openMenu}
        showOutcomes={nodeState.showOutcomes}
        setNewLinkStart={setNewLinkStart}
        setNewLinkEnd={setNewLinkEnd}
        hide={nodeState.hide}
        currentSample={!(Array.isArray(node.outcomes[0].value)) || sampleStep === null || sampleStep > node.outcomes[0].value.length ? false : node.outcomes[0].value[sampleStep][3]}
      />
      {nodeState.showLegend && !nodeState.hide ? (
        <OutcomeLegend
          id={id}
          node={node}
          x={(node.x - viewPosition.x + radius)/zoom}
          y={(node.y - viewPosition.y - radius)/zoom}
          updateGraph={updateGraph}
          sampleStep={sampleStep}
        />
      ) : null }
      {nodeState.showInfo && !nodeState.hide? (
        <NodeMenu
          id={id}
          node={node}
          x={(node.x - viewPosition.x - radius)/zoom}
          y={(node.y - viewPosition.y + radius)/zoom}
          parents={getParents(node.parents_name)}
          changeNodeName={changeNodeName}
          updateGraph={updateGraph}
          toggleShowInfo={toggleShowInfo}
        />
      ) : null}
      {menu ? (
        <RightClickMenu
          id={id}
          x={menu.x}
          y={menu.y}
          showOutcomes={nodeState.showOutcomes}
          toggleShowOutcomes={toggleShowOutcomes}
          showInfo={nodeState.showInfo}
          toggleShowInfo={toggleShowInfo}
          showLegend={nodeState.showLegend}
          toggleShowLegend={toggleShowLegend}
          hide={nodeState.hide}
          toggleHide={toggleHide}
          closeMenu={closeMenu}
          updateGraph={updateGraph}
          nodeName={node.name}
          showMarkovBlanket={showMarkovBlanket}
        />
      ) : null}
      {!node.valid ? (
        <InvalidError
          x={(node.x - viewPosition.x + (radius/2))/zoom}
          y={(node.y - viewPosition.y + (radius/2))/zoom}
        />
      ) : null}
    </Fragment>
   );
}
 
export default NodeContainer;