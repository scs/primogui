import React from 'react';
import './Link.css';

const NewLink = ({sourceX, sourceY, targetX, targetY}) => {

  const distance = Math.sqrt(Math.pow(targetX - sourceX, 2) + Math.pow(targetY - sourceY, 2));
  const angle = Math.atan2(targetY - sourceY, targetX - sourceX) * 180 / Math.PI;



  return ( 
    <g className='link' transform={`rotate(${angle} ${sourceX} ${sourceY})`}>
      <defs>
      <marker id="arrow" viewBox="0 0 10 10" refX="10" refY="5"
        markerWidth="5" markerHeight="5">
      <path d="M 0 0 L 10 5 L 0 10 z" />
      </marker>
      </defs>
      <line x1={sourceX} y1={sourceY} x2={sourceX + distance} y2={sourceY} stroke="black" stroke-width="2" markerEnd="url(#arrow)"/>
    </g>
  );
}
 
export default NewLink;