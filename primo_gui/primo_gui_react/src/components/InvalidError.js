import React, { useState } from 'react';
import ErrorIcon from '@material-ui/icons/ErrorTwoTone';
import ReactDOM from 'react-dom';

const InvalidError = ({x, y}) => {
  const [hoverError, setHoverError] = useState(false);
  
  return ReactDOM.createPortal(
    (
      <div
        style={{position: 'absolute', left: x + 'px', top: y + 'px', color: 'red', display: 'flex'}}
        onMouseOver={() => setHoverError(true)}
        onMouseOut={() => setHoverError(false)}
      >
        <ErrorIcon
        />
        {hoverError ? (
          <span style={{alignSelf: 'center', backgroundColor: 'white'}}>Node CPT invalid</span>
        ) : null}
      </div>
    ), document.getElementById('app')
  );
}
 
export default InvalidError;