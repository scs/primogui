import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';
import Input from '@material-ui/core/Input';

const useStyles = makeStyles({
  root: {
    width: 250,
  },
  input: {
    width: 100,
  },
});

export default function InputSlider(props) {
  const classes = useStyles();
  const [value, setValue] = React.useState(props.default);

  const handleSliderChange = (event, newValue) => {
    setValue(newValue);
    props.onChange(newValue)
  };

  const handleInputChange = (event) => {
    let val = props.minValue;
    if (event.target.value === '' || event.target.value < props.minValue) {
        setValue(props.minValue);
    } else {
        val = event.target.value;
        setValue(Number(event.target.value))
    }
    // setValue(event.target.value === '' ? props.minValue : Number(event.target.value));
    props.onChange(val)
  };

  const handleBlur = () => {
    if (value < props.minValue) {
      setValue(props.minValue);
    } else if (value > props.maxValue) {
      setValue(props.maxValue);
    }
  };

  return (
    <div className={classes.root} style={{margin:"10px"}}>
      <Typography id="input-slider" gutterBottom>
        {props.label}
      </Typography>
      <Grid container spacing={2}  alignItems="center">
        <Grid item xs style={{flexGrow: 0}}>
          <Slider
            value={typeof value === 'number' ? value : props.default}
            onChange={handleSliderChange}
            min={props.minValue}
            max={props.maxValue}
            aria-labelledby="input-slider"
            style={{width:100, flexGrow:0}}
          />
        </Grid>
        <Grid item>
          <Input
            className={classes.input}
            value={value}
            margin="dense"
            onChange={handleInputChange}
            onBlur={handleBlur}
            inputProps={{
              step: props.step,
              min: props.minValue,
              max: props.maxValue,
              type: 'number',
              'aria-labelledby': 'input-slider',
            }}
          />
        </Grid>
      </Grid>
    </div>
  );
}