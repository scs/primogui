from primo2.inference.exact import FactorTree
from primo2.inference.exact import VariableElimination
import primo2.inference.mcmc as mcmc
from primo2.inference import dynamic
from primo2 import networks
import functools
import copy

class InferenceMethodsBN:
    def __init__(self):
        self._bn = networks.BayesianNetwork()
        self._bn.name = 'bayesiannetwork'
        self._evidence = {}
        self._num_samples = 1000
        self._full_change = False
        self._current_inference_method = ""
        self._inference = {
            "Variable Elimination": None,
            "Factor Tree": None,
            "Gibbs Sampling": None,
            "Metropolis Hastings": None,
            "Visualise Samples": None,
        }
        self.__update()

    @property
    def current_inference_method(self):
        return self._current_inference_method

    @current_inference_method.setter
    def current_inference_method(self, value):
        self._current_inference_method = value
        self.__update()

    def __update(self):
        if self.current_inference_method == "Variable Elimination":
            self._inference.update({"Variable Elimination": functools.partial(VariableElimination.bucket_marginals,
                                                      self._bn, evidence=self._evidence)})
        elif self.current_inference_method == "Factor Tree":
            self._tree = FactorTree.create_jointree(self._bn)
            self._tree.set_evidence(self._evidence)
            self._inference.update({"Factor Tree": self._tree.marginals})
        elif self.current_inference_method == "Gibbs Sampling":
            self._mcmcGibbs = mcmc.MCMC(self._bn, transitionModel=mcmc.GibbsTransition(), numSamples=self._num_samples, fullChange=self._full_change)
            initial_state = self._bn.get_sample(self._evidence)
            self._sampleChain = list(self._mcmcGibbs.sampler.generate_markov_chain(self._bn, self._num_samples, initial_state, self._evidence))
            self._inference.update({"Gibbs Sampling": functools.partial(self._mcmcGibbs.marginals, evidence=self._evidence, sampleChain=self._sampleChain)})
        elif self.current_inference_method == "Metropolis Hastings":
            self._mcmcMetropolis = mcmc.MCMC(self._bn, transitionModel=mcmc.MetropolisHastingsTransition(), numSamples=self._num_samples, fullChange=self._full_change)
            initial_state = self._bn.get_sample(self._evidence)
            self._sampleChain = list(self._mcmcMetropolis.sampler.generate_markov_chain(self._bn, self._num_samples, initial_state, self._evidence))
            self._inference.update({"Metropolis Hastings": functools.partial(self._mcmcMetropolis.marginals, evidence=self._evidence, sampleChain=self._sampleChain)})
        elif self.current_inference_method == "Visualise Samples":
            self._mcmcGibbs = mcmc.MCMC(self._bn, transitionModel=mcmc.GibbsTransition(), numSamples=self._num_samples, fullChange=False)
            initial_state = self._bn.get_sample(self._evidence)
            self._sampledVars = []
            self._sampleChain = list(self._mcmcGibbs.sampler.generate_markov_chain(self._bn, self._num_samples, initial_state, self._evidence, stepByStep=True, sampledVars=self._sampledVars))
            self._inference.update({"Visualise Samples": functools.partial(self._mcmcGibbs.marginals, stepByStep=True, evidence=self._evidence, sampleChain=self._sampleChain, sampledVars=self._sampledVars)})
        elif self.current_inference_method != "":
            print("Unknown inference method: {}".format(self.current_inference_method))

    def get_bn(self):
        return self._bn

    def set_bn(self, bn):
        self._bn = copy.deepcopy(bn)
        self._bn.name = 'bayesiannetwork'
        self.__update()

    def get_evidence(self):
        return self._evidence

    def set_evidence(self, evidence):
        self._evidence = evidence
        self.__update()

    def set_num_samples(self, num_samples):
        self._num_samples = num_samples
        self.__update()

    def set_full_change(self, full_change):
        self._full_change = full_change
        self.__update()

    def get_inference(self):
        return self._inference

    def get_inference_types(self):
        return list(self._inference.keys())

    def infer(self, inference_method, nodes):
        print("Infer method: ", inference_method)
        return self._inference[inference_method](nodes)



class InferenceMethodsDBN:
    def __init__(self):
        self._dbn = networks.DynamicBayesianNetwork()
        self._dbn.b0.name = 'dynamicbayesiannetwork_b0'
        self._dbn.two_tbn.name = 'dynamicbayesiannetwork_2tbn'
        self._evidence = {}
        self.__update()

    def __update(self):
        self._pfb = dynamic.PriorFeedbackExact(self._dbn)
        self._pfb.set_evidence(evidence=self._evidence)
        self._se = dynamic.SoftEvidenceExact(self._dbn)
        self._se.set_evidence(evidence=self._evidence)
        self._inference = {
            'Prior Feedback': self._pfb.marginals,
            'Soft Evidence': self._se.marginals}

    def get_dbn(self):
        return self._dbn

    def set_dbn(self, dbn):
        self._dbn = copy.deepcopy(dbn)
        self._dbn.b0.name = 'dynamicbayesiannetwork_b0'
        self._dbn.two_tbn.name = 'dynamicbayesiannetwork_2tbn'
        self.__update()

    def get_b0(self):
        return self._dbn.b0

    def set_b0(self, b0):
        self._dbn.b0 = copy.deepcopy(b0)
        self._dbn.b0.name = 'dynamicbayesiannetwork_b0'
        self.__update()

    def get_tbn(self):
        return self._dbn.two_tbn

    def set_tbn(self, tbn):
        self._dbn.two_tbn = copy.deepcopy(tbn)
        self._dbn.two_tbn.name = 'dynamicbayesiannetwork_2tbn'
        self.__update()

    def get_evidence(self):
        return self._evidence

    def set_evidence(self, evidence):
        self._evidence = evidence
        self._pfb.set_evidence(evidence=self._evidence)
        self._se.set_evidence(evidence=self._evidence)

    def get_inference(self):
        return self._inference

    def get_inference_types(self):
        return list(self._inference.keys())

    def infer(self, inference_method, node):
        return self._inference[inference_method]([node])

    def get_t(self):
        return self._pfb._t

    def unroll(self):
        self._pfb.unroll(evidence=self._evidence)
        self._se.unroll(evidence=self._evidence)

    def reset(self):
        self._evidence = {}
        self.__update()
